from django.urls import path

from . import views

urlpatterns = [
    path('', views.IndexPage.as_view(), name='index'),
    path('about/', views.AboutPage.as_view(), name='about'),
]

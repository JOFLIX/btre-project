from django.urls import path

from . import views

urlpatterns = [
    path('', views.ListingView.as_view(), name='listings'),
    path('<int:pk>/', views.ListDetailView.as_view(), name='listing'),
    path('search/', views.SearchView.as_view(), name='search')
]
